<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "noticia".
 *
 * @property int $idNoticia
 * @property string|null $titular
 * @property string|null $textoCorto
 * @property string|null $textoLargo
 * @property int|null $portada
 * @property int|null $seccion
 * @property string|null $fecha
 * @property $foto
 * @property int|null $autor
 * @property Autor $autor0 
 * @property Seccion $seccion0 
 */
class Noticia extends \yii\db\ActiveRecord
{
    public $fichero; // foto subida
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idNoticia'], 'required'],
            [['idNoticia', 'seccion', 'autor'], 'integer'],
            [['portada'], 'boolean'],
            [['textoLargo'], 'string'],
            // La fecha es un date-time por que no le ponemos en las rules que sea de tipo date para que no de problemas
            [['fecha'], 'safe'],
            [['titular', 'foto'], 'string', 'max' => 255],
            [['textoCorto'], 'string', 'max' => 800],
            [['fichero'], 'file',  'skipOnEmpty' => true, 'mimeTypes' => 'image/*'],
            // 'mimeTypes' => 'image/*' usamos mimeTypes en la rules de las fotos en lugar de extensions porque extensions no funciona debido a un bug 'extensions'  => 'jpg , png'
            [['foto'], 'string'],
            [['idNoticia'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idNoticia' => 'Id Noticia',
            'titular' => 'Titular',
            'textoCorto' => 'Texto Corto',
            'textoLargo' => 'Texto Largo',
            'portada' => 'Portada',
            'seccion' => 'Seccion',
            'fecha' => 'Fecha',
            'foto' => 'Foto',
            'fichero' => 'Selecciona fichero',
            'autor' => 'Autor',
        ];
    }

    /**
     * Gets query for [[Autor0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAutor0()
    {
        return $this->hasOne(Autor::class, ['id' => 'autor']);
    }

    /** Gets query for [[Seccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSeccion0()
    {
        return $this->hasOne(Seccion::class, ['id' => 'seccion']);
    }

    public function getSecciones()
    {
        $secciones = Seccion::find()->all();
        return ArrayHelper::map($secciones, 'id', 'nombre');
    }

    public function getAutores()
    {
        $autores = Autor::find()->all();
        return ArrayHelper::map($autores, 'id', 'nombre');
    }

    public function beforeValidate(): bool
    {
        // Comprobamos si has seleccionado una foto 
        if (isset($this->fichero)) {
            $this->fichero = UploadedFile::getInstance($this, "fichero");
        }
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }

    public function afterValidate(): bool
    {
        // Volvemos a comprobar si se ha subido una foto
        if (isset($this->fichero)) {
            $this->subirArchivo();
            $this->foto = $this->idNoticia . '_' . $this->fichero->name; // Al campo foto le guardamos el nombre de la foto
        }
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }


    public function subirArchivo(): bool
    {
        $this->fichero->saveAs('imgs/noticias/' . $this->idNoticia . '_' . $this->fichero->name, false);  // false, si la foto existe, no sube la nueva para no destruir la que está
        return true;
    }

    // Disparador que se ejecuta después de eliminar un registro
    public function afterDelete()
    {
        // Elimino la imagen de la noticia de web/imgs
        // Comprobamos que exista el campo foto y posteriormente comprobamos que realmente apunte a la imagen de la carpeta
        if (isset($this->foto) && file_exists(Yii::getAlias("@webroot") . '/imgs/noticias/' . $this->foto)) {
            unlink('imgs/noticias/' . $this->foto);
        }
        return true;
    }

    /**
     * afterSave 
     * Este método se ejecuta después de guardar el registro en la BBDD
     * 
     * @param  mixed $insert este argumento es true si se está insertando un registro y false si es una actualización
     * @param  array $atributosAnteriores Array con todos los datos de la tabla antes de actualizar
     * @return void
     */
    public function afterSave($insert, $atributosAnteriores)
    {
        // Si estoy actualizando datos
        if (!$insert) {
            // Si la noticia tenia ya una foto y hemos adjuntado una nueva, elimina la vieja
            if (isset($this->fichero) && isset($atributosAnteriores['foto'])) {
                unlink('imgs/noticias/' . $atributosAnteriores['foto']);
            }
        }
    }

    public function fechaEspana()
    {
        if (isset($this->fecha)) {
            return Yii::$app->formatter->asDate($this->fecha, 'dd-MM-yyyy HH:mm');
        }
    }
}
