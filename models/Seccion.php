<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "seccion".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $foto 
 *
 * @property Noticia[] $noticias
 */
class Seccion extends \yii\db\ActiveRecord
{
    public $archivo;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'seccion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['nombre', 'foto'], 'string', 'max' => 100],
            [['archivo'], 'file',  'skipOnEmpty' => true, 'mimeTypes' => 'image/*'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'foto' => 'Foto'
        ];
    }

    /**
     * Gets query for [[Noticias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticias()
    {
        return $this->hasMany(Noticia::class, ['seccion' => 'id']);
    }

    public function beforeValidate(): bool
    {
        // Comprobamos si has seleccionado una foto 
        if (isset($this->archivo)) {
            $this->archivo = UploadedFile::getInstance($this, "archivo");
        }
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }

    public function afterValidate(): bool
    {
        // Volvemos a comprobar si se ha subido una foto
        if (isset($this->archivo)) {
            $this->subirArchivo();
            $this->foto = $this->id . '_' . $this->archivo->name; // Al campo foto le guardamos el nombre de la foto
        }
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }


    public function subirArchivo(): bool
    {
        $this->archivo->saveAs('imgs/secciones/' . $this->id . '_' . $this->archivo->name, false);  // false, si la foto existe, no sube la nueva para no destruir la que está
        return true;
    }

    // Disparador que se ejecuta después de eliminar un registro
    public function afterDelete()
    {
        // Elimino la imagen de la noticia de web/imgs
        if ($this->foto != null && file_exists(Yii::getAlias("@webroot") . '/imgs/noticias/' . $this->foto)) {
            unlink('imgs/secciones/' . $this->foto);
        }
        return true;
    }

    public function afterSave($insert, $atributosAnteriores)
    {
        // Si estoy actualizando datos
        if (!$insert) {
            // Si la noticia tenia ya una foto y hemos adjuntado una nueva, elimina la vieja
            if (isset($this->archivo) && isset($atributosAnteriores['foto'])) {
                unlink('imgs/secciones/' . $atributosAnteriores['foto']);
            }
        }
    }
}
