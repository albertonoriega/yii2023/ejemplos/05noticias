<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Noticia $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="noticia-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idNoticia')->input('number') ?>

    <?= $form->field($model, 'titular')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'textoCorto')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'textoLargo')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'portada')->checkbox() ?>

    <?= $form->field($model, 'seccion')->dropDownList($model->secciones, ['prompt' => 'Selecciona sección']) ?>

    <?= $form->field($model, 'fecha')->input('datetime-local') ?>

    <?= $form->field($model, 'fichero')->fileInput() ?>

    <?= $form->field($model, 'autor')->listBox($model->autores) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>