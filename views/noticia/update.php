<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Noticia $model */

$this->title = 'Actualizar noticia: ' . $model->titular;
$this->params['breadcrumbs'][] = ['label' => 'Noticias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->titular, 'url' => ['view', 'idNoticia' => $model->idNoticia]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="noticia-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formUpdate', [
        'model' => $model,
    ]) ?>

</div>