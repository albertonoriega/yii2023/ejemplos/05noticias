<?php

use yii\helpers\Html;
?>
<table class="tablaNoticia">
    <tr>
        <td>Titular</td>
        <td><?= $dato->titular ?></td>
    </tr>
    <tr>
        <td>Texto largo</td>
        <td><?= $dato->textoLargo ?></td>
    </tr>
    <tr>
        <td>Foto</td>
        <td><?= Html::img("@web/imgs/noticias/{$dato->foto}") ?></td>
    </tr>
    <tr>
        <td>Autor</td>
        <td> <?= $dato->autor ?></td>
    </tr>
    <tr>
        <td>Fecha</td>
        <td> <?= $dato->fecha ?></td>
    </tr>
    <tr>
        <td>Sección</td>
        <td> <?= $dato->seccion ?></td>
    </tr>

</table>