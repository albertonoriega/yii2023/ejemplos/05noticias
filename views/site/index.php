<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5 tituloPortada">
        <?= Html::img('@web/imgs/NoticiasPortada.jpg', ['class' => 'img-thumbnail', 'width' => '225px']);   ?>
        <h1 class="display-4"><?= isset($titulo) ? $titulo : "Noticias" ?></h1>
        <?= Html::img('@web/imgs/NoticiasPortada.jpg', ['class' => 'img-thumbnail', 'width' => '225px']);   ?>

    </div>


    <div class="conjuntoNoticias">
        <?php
        foreach ($datos as $dato) {
            echo $this->render('_noticia', [
                'dato' => $dato,
            ]);
        }
        ?>

    </div>
</div>