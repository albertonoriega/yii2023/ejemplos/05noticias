<?php

use yii\helpers\Html;

?>
<div class="noticia">

    <h3><?= $dato['titular'] ?></h3>


    <p><?= $dato['textoCorto'] ?></p>

    <?= Html::img("@web/imgs/noticias/{$dato['foto']}", ['width' => '150px', 'class' => 'img-thumbnail']) ?>
    <br>
    <?= Html::a('Ver noticia', ['site/vernoticia', 'idNoticia' => $dato->idNoticia], ['class' => 'botonPortada']) ?>

</div>