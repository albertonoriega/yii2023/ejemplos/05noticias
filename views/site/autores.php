<?php

use yii\helpers\Html;

?>

<div class="jumbotron text-center bg-transparent mt-5 mb-5 tituloPortada">
    <?= Html::img('@web/imgs/NoticiasPortada.jpg', ['class' => 'img-thumbnail', 'width' => '225px']);   ?>
    <h1 class="display-4">Autores</h1>
    <?= Html::img('@web/imgs/NoticiasPortada.jpg', ['class' => 'img-thumbnail', 'width' => '225px']);   ?>
</div>

<div class="row">
    <?php
    foreach ($autores as $autor) {
    ?>
        <div class="col-4 mb-3">
            <div class="card bg-warning text-dark cartas">
                <div class="imagenCard">
                    <?php
                    if (isset($autor->foto)) {
                        echo Html::img("@web/imgs/autores/{$autor->foto}", ["width" => 100, "height" => 100, 'style' => 'border-radius:50%;margin:5px']);
                    } else {
                        echo Html::img("@web/imgs/autores/anonimo.png", ["width" => 100, "height" => 100, 'style' => 'border-radius:50%;margin:5px']);
                    }
                    ?>
                </div>
                <div class="card-body">
                    <h5 class="card-title"><?= $autor->nombre ?></h5>
                    <?= Html::a('Ver noticias', ['site/autor', 'id' => $autor->id], ['class' => 'btn btn-light']) ?>

                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>