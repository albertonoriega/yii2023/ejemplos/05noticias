<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Autor $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Autores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="autor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que quieres eliminar el registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            //'foto',
            [
                'label' => 'Foto',
                'format' => 'raw',
                'value' => function ($dato) {
                    if ($dato->foto != null) {
                        return Html::img("@web/imgs/autores/{$dato->foto}", ["width" => 300, "height" => 300]);
                    } else {
                        return Html::img("@web/imgs/autores/anonimo.png", ["width" => 300, "height" => 300]);
                    }
                }
            ],
            //'fechaNacimiento',
            [
                'attribute' => 'fechaNacimiento',
                'value' => function ($model) {
                    return $model->fechaEspana();
                }
            ],
            'correo',
        ],
    ]) ?>

</div>