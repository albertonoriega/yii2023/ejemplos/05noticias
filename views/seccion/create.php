<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Seccion $model */

$this->title = 'Nueva seccion';
$this->params['breadcrumbs'][] = ['label' => 'Secciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seccion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formInsert', [
        'model' => $model,
    ]) ?>

</div>